﻿using Orleans.Streams;
using RxTests.Core;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RxTests.Orleans
{
    class OrleansStreamImpl<T> : IStream<T>
    {
        private readonly ConcurrentDictionary<string, StreamSubscriptionHandle<T>> subscriptions = new ConcurrentDictionary<string, StreamSubscriptionHandle<T>>();
        private readonly IAsyncStream<T> orleansStream;

        public OrleansStreamImpl(IAsyncStream<T> orleansStream)
        {
            this.orleansStream = orleansStream ?? throw new ArgumentNullException(nameof(orleansStream));
        }

        public async Task PublishAsync(T item)
        {
            await this.orleansStream.OnNextAsync(item);

            var handles = await this.orleansStream.GetAllSubscriptionHandles();
            Trace.WriteLine($"Published from stream {this.orleansStream.Guid} to all {handles.Count} subscribers.");
        }

        public async Task SubscribeAsync(string guid, Func<T, Task> subscription)
        {
            var handle = await this.orleansStream.SubscribeAsync((data, token) => subscription(data));
            this.subscriptions.AddOrUpdate(guid, handle, (key, oldHandle) => handle);

            var handles = await this.orleansStream.GetAllSubscriptionHandles();
            Trace.WriteLine($"New subscription on stream {this.orleansStream.Guid}: {handle.HandleId}, total subscriptions: {handles.Count}");
        }

        public async Task UnsubscribeAsync(string guid)
        {
            if(this.subscriptions.TryRemove(guid, out StreamSubscriptionHandle<T> handle))
            {
                await handle.UnsubscribeAsync();

                var handles = await this.orleansStream.GetAllSubscriptionHandles();
                Trace.WriteLine($"{handle.HandleId} unsubscribed from stream {this.orleansStream.Guid}, total subscriptions: {handles.Count}");
            }
        }
    }
}
