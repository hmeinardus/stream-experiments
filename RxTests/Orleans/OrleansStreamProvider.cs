﻿using Orleans;
using Orleans.Providers;
using Orleans.Runtime.Configuration;
using RxTests.Core;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RxTests.Orleans
{
    public class OrleansStreamProvider : IStreamProvider
    {
        private static readonly MemoryStreamProvider assemblyRef;

        readonly IClientBuilder builder;
        IClusterClient client;

        public OrleansStreamProvider()
        {
            var config = ClientConfiguration.LoadFromFile("clientconfig.xml");
            var builder = new ClientBuilder();
            this.builder = builder.UseConfiguration(config);            
        }

        public IStream<T> GetStream<T>(string streamId)
        {
            var streamProvider = this.client.GetStreamProviders().Single();
            return new OrleansStreamImpl<T>(streamProvider.GetStream<T>(Guid.Parse(streamId), Constants.STREAM_NAMESPACE));
        }

        public async Task InitializeAsync()
        {
            this.client = this.builder.Build();
            await this.client.Connect();
        }
    }
}
