﻿using RxTests.Core;
using RxTests.Orleans;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RxTests
{
    class Program
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            //var streamProvider = new StreamProvider();
            var streamProvider = new OrleansStreamProvider();
            await streamProvider.InitializeAsync();

            var client = clientFactory(streamProvider);

            //var backgroundWorker = new BackgroundStreamWorker<Request, Response>(streamProvider, req => new Response());
            //await backgroundWorker.RegisterPubSub(Constants.REQUEST_STREAM_ID, Constants.RESPONSE_STREAM_ID);

            Console.WriteLine("Press Enter to send a request.");

            while(true)
            {
                var key = Console.ReadKey();
                if(key.Key == ConsoleKey.Escape)
                {
                    break;
                }
                if (key.Key == ConsoleKey.Enter)
                {
                    var watch = Stopwatch.StartNew();
                    try
                    {
                        var response = await client.PerformRequest(new Request(), TimeSpan.FromSeconds(1));
                        watch.Stop();
                        Console.WriteLine($"Response received in {watch.ElapsedMilliseconds} ms.");
                    }
                    catch (Exception ex)
                    {
                        watch.Stop();
                        Console.WriteLine($"Exception received in {watch.ElapsedMilliseconds} ms: {ex.Message}");
                    }
                }
            }

            //backgroundWorker.Dispose();
        }

        private static IRequestResponseClient<Request,Response> clientFactory(IStreamProvider streamProvider)
        {
            //return new RequestResponseClient<Request, Response>(() => streamProvider);
            return new RxRequestResponseClient<Request, Response>(() => streamProvider);
        }
    }
}
