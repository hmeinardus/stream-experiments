﻿using RxTests.Core;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace RxTests
{
    public static class ObservableHelper
    {
        public static IObservable<T> FromStream<T>(IStream<T> stream)
        {
            return Observable.Create<T>(observer => new Subscription<T>(observer, stream));
        }
    }

    public class Subscription<T> : IDisposable
    {
        private readonly string subscriptionId;
        private readonly IStream<T> stream;

        public Subscription(IObserver<T> observer, IStream<T> stream)
        {
            this.subscriptionId = Guid.NewGuid().ToString();
            this.stream = stream ?? throw new ArgumentNullException(nameof(stream));

            this.stream.SubscribeAsync(this.subscriptionId, response =>
            {
                observer.OnNext(response);
                return Task.CompletedTask;
            });            
        }

        public async void Dispose()
        {
            await this.stream.UnsubscribeAsync(this.subscriptionId);
        }
    }
}
