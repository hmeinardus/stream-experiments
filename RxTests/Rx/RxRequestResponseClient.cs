﻿using RxTests.Core;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace RxTests
{
    public class RxRequestResponseClient<TRequest,TResponse> : IRequestResponseClient<TRequest, TResponse>
    {
        public RxRequestResponseClient(Func<IStreamProvider> GetStreamProvider)
        {
            this.GetStreamProvider = GetStreamProvider;
        }

        private Func<IStreamProvider> GetStreamProvider { get; }

        public async Task<TResponse> PerformRequest(TRequest request, TimeSpan timeout)
        {
            var streamProvider = GetStreamProvider();
            var requestStream = streamProvider.GetStream<TRequest>(Constants.REQUEST_STREAM_ID);
            var responseStream = streamProvider.GetStream<TResponse>(Constants.RESPONSE_STREAM_ID);

            var observable = ObservableHelper.FromStream(responseStream);

            var hotObservable = observable.Replay();
            
            using (hotObservable.Connect())
            {
                await requestStream.PublishAsync(request);

                return await hotObservable.Timeout(timeout).FirstAsync();
            }
        }
    }
}
