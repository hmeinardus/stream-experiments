﻿using System;
using System.Threading.Tasks;

namespace RxTests.Core
{
    public interface IStream<T>
    {
        Task PublishAsync(T item);
        Task SubscribeAsync(string guid, Func<T, Task> subscription);
        Task UnsubscribeAsync(string guid);
    }
}
