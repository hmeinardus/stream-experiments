﻿using System.Collections.Concurrent;

namespace RxTests.Core
{
    public class StreamProvider : IStreamProvider
    {
        readonly ConcurrentDictionary<string, object> streams = new ConcurrentDictionary<string, object>();
        
        public IStream<T> GetStream<T>(string streamId)
        {
            return (IStream<T>)this.streams.GetOrAdd(streamId, (object)new StreamImpl<T>(streamId));
        }
    }
}
