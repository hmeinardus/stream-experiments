﻿namespace RxTests.Core
{
    public static class Constants
    {
        public const string STREAM_NAMESPACE = "Default";

        public const string REQUEST_STREAM_ID = "{696C3A1A-5F9B-424F-9000-F2464CB34940}";
        public const string RESPONSE_STREAM_ID = "{25CE39FC-4D8A-4B95-8109-5DBB62004166}";
    }
}
