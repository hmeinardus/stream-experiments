﻿using System;
using System.Threading.Tasks;

namespace RxTests.Core
{
    public class RequestResponseClient<TRequest,TResponse> : IRequestResponseClient<TRequest, TResponse>
    {
        public RequestResponseClient(Func<IStreamProvider> GetStreamProvider)
        {
            this.GetStreamProvider = GetStreamProvider;
        }

        private Func<IStreamProvider> GetStreamProvider { get; }

        public async Task<TResponse> PerformRequest(TRequest request, TimeSpan timeout)
        {
            var streamProvider = GetStreamProvider();
            var requestStream = streamProvider.GetStream<TRequest>(Constants.REQUEST_STREAM_ID);
            var responseStream = streamProvider.GetStream<TResponse>(Constants.RESPONSE_STREAM_ID);

            var tcs = new TaskCompletionSource<TResponse>();
            var subId = Guid.NewGuid().ToString();

            await responseStream.SubscribeAsync(subId, r =>
            {
                tcs.SetResult(r);
                return Task.CompletedTask;
            });

            await requestStream.PublishAsync(request);

            var workerTask = tcs.Task;
            var timeoutTask = Task.Delay(timeout);

            await Task.WhenAny(workerTask, timeoutTask);

            await responseStream.UnsubscribeAsync(subId);

            if (!workerTask.IsCompleted)
            {
                throw new TimeoutException($"No response received within {timeout}.");
            }

            return await workerTask;
        }
    }
}
