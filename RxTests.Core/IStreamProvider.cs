﻿namespace RxTests.Core
{
    public interface IStreamProvider
    {
        IStream<T> GetStream<T>(string streamId);
    }
}
