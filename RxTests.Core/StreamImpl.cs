﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RxTests.Core
{
    public class StreamImpl<T> : IStream<T>
    {
        private readonly ConcurrentDictionary<string, Func<T, Task>> subscribers = new ConcurrentDictionary<string, Func<T, Task>>();
        private readonly string streamId;

        public StreamImpl(string streamId)
        {
            this.streamId = streamId ?? throw new ArgumentNullException(nameof(streamId));
        }

        public async Task PublishAsync(T item)
        {
            var tasks = new List<Task>();

            foreach (var sub in this.subscribers)
            {
                tasks.Add(sub.Value(item));
            }

            await Task.WhenAll(tasks);
            Trace.WriteLine($"Published from stream {this.streamId} to all {this.subscribers.Count} subscribers.");
        }

        public Task SubscribeAsync(string guid, Func<T, Task> subscription)
        {
            this.subscribers.AddOrUpdate(guid, subscription, (key, oldValue) => subscription);
            Trace.WriteLine($"New subscription on stream {this.streamId}: {guid}, total subscriptions: {this.subscribers.Count}");
            return Task.CompletedTask;
        }

        public Task UnsubscribeAsync(string guid)
        {
            this.subscribers.TryRemove(guid, out _);
            Trace.WriteLine($"{guid} unsubscribed from stream {this.streamId}, total subscriptions: {this.subscribers.Count}");
            return Task.CompletedTask;
        }
    }
}
