﻿using System;
using System.Threading.Tasks;

namespace RxTests.Core
{
    public interface IRequestResponseClient<TRequest, TResponse>
    {
        Task<TResponse> PerformRequest(TRequest request, TimeSpan timeout);
    }
}