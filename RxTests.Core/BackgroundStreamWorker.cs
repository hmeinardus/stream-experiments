﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace RxTests.Core
{
    public class BackgroundStreamWorker<T1, T2> : IDisposable
    {
        private readonly IStreamProvider streamProvider;
        private readonly Func<T1, T2> valueFactory;
        private readonly ConcurrentDictionary<string, IStream<T1>> subscriptions = new ConcurrentDictionary<string, IStream<T1>>();

        public BackgroundStreamWorker(IStreamProvider streamProvider, Func<T1,T2> valueFactory)
        {
            this.streamProvider = streamProvider ?? throw new ArgumentNullException(nameof(streamProvider));
            this.valueFactory = valueFactory ?? throw new ArgumentNullException(nameof(valueFactory));
        }

        public async void Dispose()
        {
            foreach (var sub in this.subscriptions)
            {
                await sub.Value.UnsubscribeAsync(sub.Key);
            }
        }

        public async Task RegisterPubSub(string stream1Id, string stream2Id)
        {
            var subStream = this.streamProvider.GetStream<T1>(stream1Id);
            var pubStream = this.streamProvider.GetStream<T2>(stream2Id);

            var subscriptionId = Guid.NewGuid().ToString();

            await subStream.SubscribeAsync(subscriptionId, async x =>
            {
                await pubStream.PublishAsync(valueFactory(x));
            });

            this.subscriptions.AddOrUpdate(subscriptionId, subStream, (id, oldStream) => subStream);
        }
    }
}
