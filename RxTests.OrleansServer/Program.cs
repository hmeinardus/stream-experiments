﻿using Orleans.Providers;
using Orleans.Runtime.Configuration;
using Orleans.Runtime.Host;
using System;

namespace RxTests.OrleansServer
{
    class Program
    {
        private static readonly MemoryStreamProvider assemblyRef;

        static void Main(string[] args)
        {
            var config = new ClusterConfiguration();
            config.LoadFromFile("clusterconfig.xml");

            using (var siloHost = new SiloHost("RxTestsSilo", config))
            {
                siloHost.InitializeOrleansSilo();
                siloHost.StartOrleansSilo();

                Console.WriteLine("Press Enter to cancel.");
                Console.ReadLine();

                siloHost.ShutdownOrleansSilo();
            }
        }
    }
}
