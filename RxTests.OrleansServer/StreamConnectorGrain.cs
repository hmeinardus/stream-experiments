﻿using Orleans;
using RxTests.Core;
using System;
using System.Linq;
using System.Threading.Tasks;
using Orleans.Streams;

namespace RxTests.OrleansServer
{
    [ImplicitStreamSubscription(Constants.STREAM_NAMESPACE)]
    public class StreamConnectorGrain : Grain, IStreamConnector
    {
        public override async Task OnActivateAsync()
        {
            var streamProvider = this.GetStreamProviders().Single();
            var requestStream = streamProvider.GetStream<Request>(Guid.Parse(Constants.REQUEST_STREAM_ID), Constants.STREAM_NAMESPACE);
            var responseStream = streamProvider.GetStream<Response>(Guid.Parse(Constants.RESPONSE_STREAM_ID), Constants.STREAM_NAMESPACE);

            await requestStream.SubscribeAsync(async (request, token) =>
            {
                await responseStream.OnNextAsync(new Response(), token);
            });

            await base.OnActivateAsync();
        }
    }
}
